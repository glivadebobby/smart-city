import { Component, NgZone, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { AuthServiceProvider } from '../../app/services/index';
declare var google: any;
@IonicPage({
  segment: 'map'
})
@Component({
  selector: 'map',
  templateUrl: 'map.html'
})
export class MapPage {
  place: any = {};
  text: string;
  map;
  marker;
  location = { lat: 13.035156899999999, lon: 80.1952657 }
  constructor(public ngZone: NgZone, public nav: NavParams, public global: AuthServiceProvider, public viewCtrl: ViewController) {
    var location = this.nav.get('location');
    if (location) {
      location = JSON.parse(location);
      if (Object.keys(location).length > 0) {
        this.location = location;
      }
    }
    setTimeout(() => {
      this.loadMap(this.location)
    }, 500);
  }

  loadMap(location) {
    console.log(this.location);
    console.log(document.getElementById('address'));
    this.place.lat = location.lat;
    this.place.lon = location.lon;
    this.initMap(location);
    setTimeout(() => {
      this.initPlaceSearch();
    }, 100)
  }

  initPlaceSearch() {
    var input = document.getElementById('address');
    let autocomplete = new google.maps.places.Autocomplete(input, {
      types: ["address"],
    });
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        //get the place result
        let place = google.maps.places.PlaceResult = autocomplete.getPlace();

        //verify result
        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        setTimeout(() => {
          this.place.lat = place.geometry.location.lat();
          this.place.lon = place.geometry.location.lng();
          console.log(place)
          this.map.setCenter(place.geometry.location)
          this.marker.setPosition(place.geometry.location);
          if(place.formatted_address){
            this.place.address = place.formatted_address;
          }
        }, 0);
      })
    });
  }

  initMap(location) {
    let latLng = new google.maps.LatLng(location.lat, location.lon);
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.map = new google.maps.Map(document.getElementById('map'), mapOptions);
    this.addMarker(this.map)
    this.getCurrentAddress(latLng).then((res: string) => {
      this.place.address = res;
      document.getElementById('address').textContent = res;
      console.log(this.place)
    })

    // google.maps.event.addListenerOnce(this.map, 'idle', () => {
    //   const event = new Event('resize');
    //   window.dispatchEvent(event);
    // });
  }

  getCurrentAddress(location) {
    return new Promise((resolve, reject) => {
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({
        'location': location
      }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          resolve(results[0].formatted_address)
        } else {
          reject(status);
        }
      });
    })
  }

  addMarker(map) {
    var GoogleMapsEvent = google.maps.event;
    this.marker = new google.maps.Marker({
      map: map,
      animation: google.maps.Animation.DROP,
      position: this.map.getCenter(),
      draggable: true
    })
    this.marker.addListener('dragend', (event) => {
      this.place.lat = event.latLng.lat();
      this.place.lon = event.latLng.lng();
      this.getCurrentAddress(event.latLng).then((res: string) => {
        console.log(res);
        this.place.address = res;
        document.getElementById('address').textContent = res;
      })
    });
  }

  submitOptions() {
    this.viewCtrl.dismiss(this.place);
  }

}
