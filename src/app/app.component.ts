import { Component,ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MenuController,NavController } from 'ionic-angular';
import { AuthServiceProvider,StorageService } from './../app/services';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  // rootPage:any = '';
  @ViewChild('rootNavCtrl') public nav: NavController;
  constructor(platform: Platform, statusBar: StatusBar,  public storage : StorageService, public global : AuthServiceProvider, splashScreen: SplashScreen, public menu : MenuController) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      this.storage.initDB().then(() =>{
        this.storage.fetchObject('auth').then((res:any) =>{
          console.log(res);
          if(Object.keys(res).length>0){
            this.global.authData = res;
            if(platform.is("cordova")){
              this.global.enableLocation();
            }
            if(this.global.authData.role == 2){
              this.nav.setRoot("SearchCityPage");
            }else{
              this.nav.setRoot("ListPlacesPage");
            }
          }else{
            this.nav.setRoot("LoginPage")
            this.global.authData = {};
          }
        }).catch(err =>{
          splashScreen.hide();
          alert(JSON.stringify(err))
          this.global.authData = {};
        })
      }).catch(err =>{
        console.log(err);
      })
    });
  }
  
  signOut(){
    this.nav.setRoot('LoginPage');
  }
}

