import { AuthServiceProvider, StorageService } from './services';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { SQLite } from '@ionic-native/sqlite';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { LaunchNavigator } from '@ionic-native/launch-navigator';

import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { DatePicker } from '@ionic-native/date-picker';


import { MyApp } from './app.component';
// import { ModelSelectPage } from './../pages/model-select/model-select';
import {PostReviewComponent} from './../pages/review-post/post-review'

@NgModule({
  declarations: [
    MyApp,
    PostReviewComponent,
    // ModelSelectPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PostReviewComponent,
    // ModelSelectPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DatePicker,
    SQLite,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthServiceProvider,
    StorageService,
    Geolocation,
    LocationAccuracy,
    LaunchNavigator
  ]
})
export class AppModule { }