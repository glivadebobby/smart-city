import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import 'rxjs/add/operator/map';
declare var cordova;
// const cordova = require('cordova.plugins.diagnostic')
const DB_NAME: string = 'myDB';
const win: any = window;

@Injectable()
export class StorageService {
    public _db: any;
    public loading: any;
    public addmodal: any;

    constructor(public platform: Platform, public sqlite: SQLite) {

    }

    set = function (key, value) {
        window.localStorage[key] = value;
    }

    get = function (key, defaultValue) {
        return window.localStorage[key] || defaultValue;
    }

    setObject = function (key, value: any) {
        window.localStorage[key] = JSON.stringify(value);
        this.fetchObject(key).then(data => {
            if (data != undefined && Object.keys(data).length != 0) {
                this.query("UPDATE smart_auth SET session_data = ? WHERE key_name = ?", [JSON.stringify(value), key])
            } else {
                this.query("INSERT INTO smart_auth(key_name,session_data) VALUES(?,?)", [key, JSON.stringify(value)])
            }
        })
    }

    fetchObject(key) {
        return new Promise((resolve, reject) => {
            this.querySelect("SELECT session_data from smart_auth WHERE key_name= ?", [key])
                .then(result => {
                    var data: any;
                    if (result.length == 0)
                        data = {};
                    else
                        data = JSON.parse(result[0].session_data)
                    resolve(data);
                }, (err) => {
                    reject("Authentication Failed. Please try again later");
                });
        });

    }
    putObject = function (key, value) {
        window.localStorage[key] = JSON.stringify(value);
    }

    getObject = function (key) {
        var data = window.localStorage[key];

        if (typeof data == 'string' && data != 'undefined')
            data = JSON.parse(data);
        return (data || {});
    }

    remove = function (key) {
        window.localStorage[key] = undefined;
    }

    clear = function (key) {
        window.localStorage.clear();
    }

    storeRootData(value: Object) {
        this.setObject('rootData', value);
    }

    initDB() {
        return new Promise((resolve, reject) => {
            if (this.platform.is('cordova')) {
                // cordova.plugins.diagnostic.getLocationMode(function (locationMode) {
                //     switch (locationMode) {
                //         case cordova.plugins.diagnostic.locationMode.HIGH_ACCURACY:
                //             console.log("High accuracy");
                //             alert("High accuracy")
                //             break;
                //         case cordova.plugins.diagnostic.locationMode.BATTERY_SAVING:
                //             console.log("Battery saving");
                //             alert("Battery saving")
                //             break;
                //         case cordova.plugins.diagnostic.locationMode.DEVICE_ONLY:
                //             console.log("Device only");
                //             alert("Device only")
                //             break;
                //         case cordova.plugins.diagnostic.locationMode.LOCATION_OFF:
                //             console.log("Location off");
                //             break;
                //     }
                // }, function (error) {
                //     console.error("The following error occurred: " + error);
                // });
                // cordova.plugins.diagnostic.requestLocationAuthorization(function (status) {
                //     switch (status) {
                //         case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                //             alert("Permission not requested");
                //             break;
                //         case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                //             alert("Permission granted");
                //             break;
                //         case cordova.plugins.diagnostic.permissionStatus.DENIED:
                //             alert("Permission denied");
                //             break;
                //         case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
                //             alert("Permission permanently denied");
                //             break;
                //     }
                // }, function (error) {
                //     console.error(error);
                // });
                this.sqlite.create({
                    name: DB_NAME,
                    location: 'default' // the location field is required
                }).then((dbObj: SQLiteObject) => {
                    this._db = dbObj;
                    // this.createTables();
                    this.query("CREATE TABLE IF NOT EXISTS smart_auth (key_name text, session_data text)").then(() => {
                        resolve();
                    }).catch(err => {
                        reject(err);
                        console.error('Storage: Unable to create initial storage tables', err.tx, err.err);
                    });
                    console.log(this._db);
                }).catch(err => {
                    reject(err);
                })
            } else {
                console.warn('Storage: SQLite plugin not installed, falling back to WebSQL. Make sure to install cordova-sqlite-storage in production!');
                this._db = win.openDatabase(DB_NAME, '1.0', 'database', 5 * 1024 * 1024);
                this.query("CREATE TABLE IF NOT EXISTS smart_auth (key_name text, session_data text)").then(() => {
                    resolve();
                }).catch(err => {
                    console.error('Storage: Unable to create initial storage tables', err.tx, err.err);
                    reject(err);
                });
            }

        })
    }

    createTables() {
        // this.query("DROP TABLE smart_auth")
        // return
        this.query("CREATE TABLE IF NOT EXISTS smart_auth (key_name text, session_data text)").catch(err => {
            console.error('Storage: Unable to create initial storage tables', err.tx, err.err);
        });

        console.log(this._db);
    }

    query(query: string, params: any[] = []): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                this._db.transaction((tx: any) => {
                    tx.executeSql(query, params,
                        (tx: any, res: any) => resolve({ tx: tx, res: res }),
                        (tx: any, err: any) => reject({ tx: tx, err: err }));
                },
                    (err: any) => reject({ err: err }));
            } catch (err) {
                reject({ err: err });
            }
        });
    }

    querySelect(query: string, params: any[] = []): Promise<any> {
        return this.query(query, params).then(data => {
            let result = [];
            if (data.res.rows.length > 0) {
                for (var cnt = 0; cnt < data.res.rows.length; cnt++) {
                    result.push(data.res.rows.item(cnt));
                }
            }
            return result;
        });
    }

}
