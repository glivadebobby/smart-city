import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { Platform } from 'ionic-angular/platform/platform';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { PopoverController, ModalController, AlertController, LoadingController, ToastController, MenuController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { StorageService } from './index';
declare var launchnavigator;


@Injectable()
export class AuthServiceProvider {
  headers;
  authData: any = {};
  userLocation = { lat: 17.9195856, lon: 77.51436289999992 }
  APIURL = "http://smartcity.glivade.in/";
  constructor(public http: Http, public platform: Platform, private launchNavigator: LaunchNavigator, private locationAccuracy: LocationAccuracy, public storage: StorageService, private geolocation: Geolocation, public popupCtrl: PopoverController, public menuCtrl: MenuController, public modalCtrl: ModalController, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
    this.init()
  }

  init() {
    this.getUserLocation();
  }
  postService(options: object): any {
    return new Promise((resolve, reject) => {
      this.headers = new Headers();
      this.headers.append('Content-Type', 'multipart/form-data');
      this.http.post(this.APIURL, options, this.headers)
        .subscribe((res: any) => {
          resolve(res._body ? res.json() : res);
        }, (err: any) => {
          reject(err._body ? err.json() : err);
        });
    });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  getUserLocation() {
    return new Promise((resolve, reject) => {
      this.geolocation.getCurrentPosition().then((resp) => {
        this.userLocation.lat = resp.coords.latitude;
        this.userLocation.lon = resp.coords.longitude;
        resolve(this.userLocation);
        console.log(this.userLocation)
      }).catch((error) => {
        reject(false)
        console.log('Error getting location', error);
      });
    })
  }
  ionicPopup(page, data: any = {}) {
    let popup = this.popupCtrl.create(page, data);
    popup.present();
    return new Promise((resolve, reject) => {
      popup.onDidDismiss((data) => {
        if (data) {
          resolve(data);
        } else {
          reject(false);
        }
      });
    })
  }

  modalOpen(pageName: string, params: any): any {
    return new Promise((resolve, reject) => {
      let addmodal = this.modalCtrl.create(pageName, params);
      addmodal.onDidDismiss((data) => {
        if (data) {
          resolve(data);
        } else {
          resolve(false);
        }
      });
      addmodal.present();
    });
  }

  enableLocation() {
    this.platform.ready().then(() => {
      this.locationAccuracy.canRequest().then((canRequest: boolean) => {
        if (canRequest) {
          // the accuracy option will be ignored by iOS
          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
            () => {
              this.getUserLocation();
            },
            error => console.log('Error requesting location permissions' + JSON.stringify(error))
          );
        }
      })
    });
  }

  openMapApp(start,end) {
    console.log(start)
    console.log(end)
    // var options: LaunchNavigatorOptions;
    // this.launchNavigator.isAppAvailable(this.launchNavigator.APP.GOOGLE_MAPS).then(res => {
    //   console.log(res);
    //   // alert(JSON.stringify(res));
    //   if(res){
    //     options = {
    //       start: [start.latitude,start.longitude],
    //       app: this.launchNavigator.APP.GOOGLE_MAPS
    //     }
    //   }else{
    //     options = {
    //       start: [start.latitude,start.longitude],
    //       app: this.launchNavigator.APP.USER_SELECT
    //     }
    //   }
    // }).catch(err => { console.log(err) });

    // let options: LaunchNavigatorOptions = {
    //   start: [start.latitude,start.longitude],
    //   app: this.launchNavigator.APP.GOOGLE_MAPS
    // };
    

    // this.launchNavigator.navigate([end.latitude,end.longitude], options)
    //   .then(
    //     success => console.log('Launched navigator'),
    //     error => console.log('Error launching navigator', error)
    //   );
    launchnavigator.isAppAvailable(launchnavigator.APP.GOOGLE_MAPS, function (isAvailable) {
      var app;
      if (isAvailable) {
        app = launchnavigator.APP.GOOGLE_MAPS;
      } else {
        console.warn("Google Maps not available - falling back to user selection");
        app = launchnavigator.APP.USER_SELECT;
      }

      launchnavigator.navigate([end.latitude, end.longitude], {
        start: start.latitude + "," + start.longitude,
        app: app
      });
    });

  }
}
