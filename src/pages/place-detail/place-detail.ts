import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from './../../app/services';
import { PostReviewComponent } from '../review-post/post-review';
declare var google;
@IonicPage({
  segment: 'place-detail/:place'
})
@Component({
  selector: 'page-place-detail',
  templateUrl: 'place-detail.html',
})
export class PlaceDetailPage {
  params: any = {};
  startPlace: any = {}
  listPlaces = [];
  place: any = {};
  text: string;
  map;
  directionsDisplay: any;
  directionsService: any;

  marker;
  location = { lat: 13.035156899999999, lon: 80.1952657 }
  constructor(public navCtrl: NavController, public ngZone: NgZone, public global: AuthServiceProvider, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.params = JSON.parse(this.navParams.get('place'));
    console.log(this.params)
    this.retrivePlaces();
  }

  retrivePlaces() {
    var data = new FormData();
    data.append('tag', 'search');
    data.append('start_time', this.params.startDate + ' ' + this.params.startTime);
    data.append('end_time', this.params.endDate + ' ' + this.params.endTime)
    data.append('latitude', this.params.latitude);
    data.append('longitude', this.params.longitude);
    this.global.postService(data).then((res: any) => {
      console.log(res);
      var location = {
        lat: this.params.latitude,
        lon: this.params.longitude
      }
      this.listPlaces = res;
      this.loadMap(location);
      this.startPlace = res[0];
    }).catch(err => { console.log(err) })
  }

  showReviews(place) {
    this.navCtrl.push("ReviewsPage", { id: place.id });
  }

  logout() {
    this.global.storage.query("DELETE FROM smart_auth").then(res => {
      this.navCtrl.setRoot('LoginPage');
    })
  }

  loadMap(location) {
    this.initMap(location);
  }

  openMapApp(endPlace){
    var startPlace = {
      lat: this.params.latitude,
      lon: this.params.longitude
    }
    this.global.openMapApp(startPlace,endPlace);
  }

  initMap(location) {
    let latLng = new google.maps.LatLng(location.lat, location.lon);
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer({
      draggable: false
    });
    let mapOptions = {
      center: latLng,
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.map = new google.maps.Map(document.getElementById('map'), mapOptions);
    this.directionsDisplay.setMap(this.map);
    this.calcRoute();
  }

  calcRoute() {
    var start = new google.maps.LatLng(this.params.latitude,this.params.longitude);
    var end = new google.maps.LatLng(this.listPlaces[this.listPlaces.length - 1].latitude, this.listPlaces[this.listPlaces.length - 1].longitude);
    var waypts = [];
    this.listPlaces.forEach(res => {
      waypts.push({
        location: new google.maps.LatLng(res.latitude, res.longitude),
        stopover: true
      });
    })

    var request = {
      origin: start,
      destination: start,
      waypoints: waypts,
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING
    };
    this.directionsService.route(request, (response, status) => {
      if (status == google.maps.DirectionsStatus.OK) {
        this.directionsDisplay.setDirections(response);
        var route = response.routes[0];
        console.log(route)
      }
    });
  }

}
