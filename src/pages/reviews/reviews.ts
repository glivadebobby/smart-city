import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from './../../app/services';
import { PostReviewComponent } from '../review-post/post-review';


@IonicPage({
  segment: 'reviews/:id'
})
@Component({
  selector: 'page-reviews',
  templateUrl: 'reviews.html',
})
export class ReviewsPage {

  placeId;
  reviews = [];
  myReview: any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams, public global: AuthServiceProvider) {
    this.placeId = this.navParams.get('id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReviewsPage');
    this.retreiveReviews();
  }

  retreiveReviews(){
    var data = new FormData();
    data.append('tag', 'reviews');
    data.append('id', this.placeId);
    data.append('user_id', this.global.authData.id);
    this.global.postService(data).then(res => {
      console.log(res);
      this.reviews = res.reviews;
      this.myReview = res.my_review;
    }).catch(err => { console.log(err) })
  }

  showConfirm(type,review) {
    this.global.ionicPopup(PostReviewComponent,{review:review}).then((data: any) => {
      console.log(data);
      this.postReview(data,type);
    }, err => {
      console.log('cancel');
    });
  }

  deleteReview(review) {
    let confirm = this.global.alertCtrl.create({
      title: 'Delete Reviews',
      message: 'Do you agree to delete this review?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            var data = new FormData();
            data.append("tag", "delete_review");
            data.append("id", review.id);
            this.global.postService(data).then((res: any) => {
              console.log(res);
              this.retreiveReviews();
              this.global.presentToast(res.message);
            }).catch(err => { console.log(err) })

            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  postReview(data,type) {
    var query = new FormData();
    if(type == 2){
      query.append('tag', 'edit_review');
      query.append('id', data.id);
    }else{
      query.append('tag', 'add_review');
      query.append('user_id', this.global.authData.id);
      query.append('place_id', this.placeId);
    }
    query.append('rating', data.rating);
    query.append('review', data.comments);
    this.global.postService(query).then(res => {
      console.log(res);
      this.retreiveReviews();
      this.global.presentToast(res.message);
    })
  }

  logout() {
    this.global.storage.query("DELETE FROM smart_auth").then(res => {
      this.navCtrl.setRoot('LoginPage');
    })
  }

}
