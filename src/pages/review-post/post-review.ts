import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'post-review',
  templateUrl: 'post-review.html'
})
export class PostReviewComponent {

  text: string;
  params: any;
  star = 0;
  id;
  review;
  homePlayersAry;
  awayPlayersAry;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
   this.review = this.navParams.get('review');
   if(this.review){
     this.text =  this.review.review;
     this.star = this.review.rating;
     this.id = this.review.id;
   }
  }

  close() {
    this.viewCtrl.dismiss();
  }

  changeRating(star) {
    this.star = star;
  }

  save(data) {
    if (data || this.star != 0) {
      this.viewCtrl.dismiss({ comments: data, rating: this.star, id: this.id});
    }
  }

}
