import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlacemanagePage } from './placemanage';

@NgModule({
  declarations: [
    PlacemanagePage,
  ],
  imports: [
    IonicPageModule.forChild(PlacemanagePage),
  ],
})
export class PlacemanagePageModule {}
