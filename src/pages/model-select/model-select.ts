import { Component } from '@angular/core';
import {NavController, NavParams,IonicPage, ViewController } from 'ionic-angular';

@IonicPage({
  segment:'pageselect'
})
@Component({
  selector: 'page-model-select',
  templateUrl: 'model-select.html',
})
export class ModelSelectPage {
  options:any = [];
  searchItem = '';
  isArrayType;
  isMultiple;
  selectedOptions = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.options = this.navParams.get('options');
    this.isMultiple = this.navParams.get('isMultiple');
    this.isArrayType = typeof this.options[0] !== 'object' ? true : false;
  }
  
  ionViewDidLoad() {
    this.options.forEach( value =>{
      if(value.checked){
        this.selectedOptions.push(value.name);
      }
    })
  }

  getOption(option){
    this.viewCtrl.dismiss(option);
  }

  getMultipleOptions(option){
    if(option.checked){
      option.checked = false;
      this.selectedOptions.splice(this.selectedOptions.indexOf(option.name),1);
    }else{
      this.selectedOptions.push(option.name);
      option.checked = true;
    }
  }

  submitOptions(){
    let options = this.selectedOptions;
    // this.selectedOptions = [];
    this.viewCtrl.dismiss(options);
  }

  onInputGet(event:any){
    this.options = this.navParams.get('options');
    let val;
    if(this.isArrayType){
      val = event.target.value;
      if (val && val.trim() != '') {
        this.options = this.options.filter((item:any) => {
          return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    }else{
       val = event.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
          this.options = this.options.filter((item:any) => {
            return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
          })
        }
    }
  }
  onCancel(){
    this.searchItem = '';
    this.options = this.navParams.get('options');
  }
}
