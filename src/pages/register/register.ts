import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from './../../app/services';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  createSuccess = false;
  registerCredentials = {name : '', mobile: '', password: '' };
  constructor(private nav: NavController, private global : AuthServiceProvider) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  public register(register) {
    var postData = new FormData();
    postData.append('tag','register');
    postData.append('name',register.name);
    postData.append('mobile',register.mobile);
    postData.append('password',register.password);
    this.global.postService(postData).then((res:any) =>{
      console.log(res);
      if(res.error == 0){
        this.nav.setRoot('LoginPage');
      }
      this.global.presentToast(res.message);
    }).catch((err) =>{
      console.log(err);
    })
  }
}
