import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from './../../app/services';
declare var google: any;

@IonicPage({
  segment: 'edit-places/:place'
})
@Component({
  selector: 'page-edit-places',
  templateUrl: 'edit-places.html',
})
export class EditPlacesPage {

  place: any = {};
  constructor(public navCtrl: NavController, public ngZone: NgZone, public navParams: NavParams, public global: AuthServiceProvider) {
  }
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('input') address: ElementRef;
  map: any;
  directionDisplay;

  ionViewDidLoad() {
    this.place = JSON.parse(this.navParams.get('place'))
    console.log(this.place)
    var location = {
      lat: this.place.latitude,
      lon: this.place.longitude,
    }
    this.loadMap(location);
  }

  loadMap(location) {
    this.place.lat = location.lat;
    this.place.lon = location.lon;
    let latLng = new google.maps.LatLng(location.lat, location.lon);
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    var input = document.getElementById('address');
    let autocomplete = new google.maps.places.Autocomplete(input, {
      types: ["address"],
    });
    // autocomplete.addListener("place_changed", () => {
    //   this.ngZone.run(() => {
    //     //get the place result
    //     let place = google.maps.places.PlaceResult = autocomplete.getPlace();

    //     //verify result
    //     if (place.geometry === undefined || place.geometry === null) {
    //       return;
    //     }
    //     var location = {
    //       lat : place.geometry.location.lat(),
    //       lng : place.geometry.location.lng()
    //     }
    //     this.loadMap(location);
    //   })
    // });

    this.getCurrentAddress(latLng).then(res => {
      this.place.address = res;
    })
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      const event = new Event('resize');
      window.dispatchEvent(event);
    });
    this.addMarker();
  }

  getCurrentAddress(location) {
    return new Promise((resolve, reject) => {
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({
        'location': location
      }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          resolve(results[0].formatted_address)
        } else {
          reject(status);
        }
      });
    })
  }

  addMarker() {
    var GoogleMapsEvent = google.maps.event;
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: this.map.getCenter(),
      draggable: true
    })
    marker.addListener('dragend', (event) => {
      console.log(event.latLng.lat())
      console.log(event.latLng.lng())
      this.place.latitude = event.latLng.lat();
      this.place.longitude = event.latLng.lng();
      this.getCurrentAddress(event.latLng).then(res => {
        console.log(res);
        this.place.address = res;
      })
    });
  }

  addInfoWindow(marker, content) {

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });

  }

  addPlace(place) {
    var data = new FormData();
    data.append("tag", "edit_place");
    data.append("name", place.name);
    data.append("id", place.id);
    data.append("address", place.address);
    data.append("latitude", place.latitude);
    data.append("longitude", place.longitude);
    data.append("stay_time", place.stay_time);
    this.global.postService(data).then((res: any) => {
      console.log(res);
      this.global.presentToast(res.message)
    }).catch(err => {
      console.log(err);
    })
  }

  logout() {
    this.global.storage.query("DELETE FROM smart_auth").then(res => {
      this.navCtrl.setRoot('LoginPage');
    })
  }

}
