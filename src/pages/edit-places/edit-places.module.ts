import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditPlacesPage } from './edit-places';

@NgModule({
  declarations: [
    EditPlacesPage,
  ],
  imports: [
    IonicPageModule.forChild(EditPlacesPage),
  ],
})
export class EditPlacesPageModule {}
