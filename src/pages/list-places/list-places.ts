import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthServiceProvider } from './../../app/services';


@IonicPage({
  segment: 'listplaces'
})
@Component({
  selector: 'page-list-places',
  templateUrl: 'list-places.html',
})
export class ListPlacesPage {

  places = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public global: AuthServiceProvider) {
  }

  ionViewDidLoad() {
    console.log(11)
    this.retrievePlaces();
  }

  retrievePlaces() {
    var data = new FormData();
    data.append("tag", "places");
    this.global.postService(data).then((res: any) => {
      console.log(res);
      this.places = res;
    }).catch(err => { console.log(err) })
  }

  showConfirm(place) {
    let confirm = this.alertCtrl.create({
      title: 'Delete Places',
      message: 'Do you agree to delete this place?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            var data = new FormData();
            data.append("tag", "delete_place");
            data.append("id", place.id);
            this.global.postService(data).then((res: any) => {
              console.log(res);
              this.retrievePlaces();
              this.global.presentToast(res.message);
            }).catch(err => { console.log(err) })

            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  goEditPlace(place) {
    this.navCtrl.push("EditPlacesPage", { place: JSON.stringify(place) })
  }

  // openMapApp(place){
  //   this.global.openMapApp(place);
  // }

  logout() {
    this.global.storage.query("DELETE FROM smart_auth").then(res => {
      this.navCtrl.setRoot('LoginPage');
    })
  }

}
