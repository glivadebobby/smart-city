import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from '../../app/services/index';

@IonicPage({
  segment : 'places/:place'
})
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  places = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public global:AuthServiceProvider) {
  }

  ionViewDidLoad() {
    this.places = JSON.parse(this.navParams.get('place'));
    console.log(this.places)
    console.log('ionViewDidLoad DashboardPage');
  }

  goPlaceDetail(place){
    this.navCtrl.push('PlaceDetailPage',{place : JSON.stringify(place)})
  }

  logout() {
    this.global.storage.query("DELETE FROM smart_auth").then(res => {
      this.navCtrl.setRoot('LoginPage');
    })
  }

}
