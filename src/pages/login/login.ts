import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, Loading, IonicPage } from 'ionic-angular';
import { AuthServiceProvider, StorageService } from './../../app/services';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loading: Loading;
  registerCredentials = { mobile: '', password: '' };

  constructor(private nav: NavController,private storage : StorageService,  private global: AuthServiceProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  public createAccount() {
    this.nav.push('RegisterPage');
  }

  public login() {
    var postData = new FormData();
    postData.append("tag", "login");
    postData.append("mobile", this.registerCredentials.mobile);
    postData.append("password", this.registerCredentials.password);
    this.global.postService(postData).then((res: any) => {
      console.log(res);
      if (res.error == 0) {
        if (res.role == 1) {
          this.global.authData.isAdmin = true;
          this.nav.setRoot('ListPlacesPage');
        } else {
          this.global.authData.isAdmin = false;
          this.nav.setRoot('SearchCityPage');
        }
        this.storage.setObject('auth', res);
      } else {
        this.global.presentToast(res.message);
      }
    }).catch(err => {
      console.log(err);
    })
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
}
