import { Component } from '@angular/core';
import { DatePicker } from '@ionic-native/date-picker';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AuthServiceProvider } from './../../app/services';

@IonicPage({
  segment: 'search-place'
})
@Component({
  selector: 'page-search-city',
  templateUrl: 'search-city.html',
})
export class SearchCityPage {
  source: any = {};
  destination: any = {};
  search: any = {};
  places = [];
  timeFlg = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, private global: AuthServiceProvider, private modalCtrl: ModalController, private datePicker: DatePicker) {
  }

  ionViewDidLoad() {
    var data = new FormData();
    data.append('tag', 'places')
    this.global.postService(data).then((res: any) => {
      console.log(data);
      this.places = res;
    }).catch(err => { console.log(err) })
  }

  endDateChange(event) {
    console.log(event)
    return false;
  }

  goSearch(search) {
    if (search.startDate && search.endDate && search.startTime && search.endTime && search.address) {
      var sdate = search.startDate;
      var edate = search.endDate;
      if (edate >= sdate) {
        console.log(edate == sdate);
        console.log(edate);
        console.log(sdate);
        if (edate == sdate) {
          var diff = search.endTime.split(':')[0] - search.startTime.split(':')[0];
          if (diff >= 1) {
            this.timeFlg = false;
            this.getPlace(search);
          } else {
            this.timeFlg = true;
          }
        } else {
          this.getPlace(search);
        }
      } else {
        this.timeFlg = true;
      }
    } else {
      search.startDate = search.startDate || ''
      search.startTime = search.startTime || '';
      search.endDate = search.endDate || '';
      search.endTime = search.endTime || '';
      search.address = search.address || '';
    }
  }

  getPlace(search) {
    this.timeFlg = false;
    var data = new FormData();
    data.append('tag', 'search')
    data.append('start_time', search.startDate + ' ' + search.startTime);
    data.append('end_time', search.endDate + ' ' + search.endTime);
    data.append('latitude', search.latitude);
    data.append('longitude', search.longitude);
    this.global.postService(data).then((res: any) => {
      console.log(res);
      this.navCtrl.push('PlaceDetailPage', { place: JSON.stringify(search) });
    }).catch(err => { console.log(err) })
  }

  openMap() {
    this.global.modalOpen('MapPage', { location: JSON.stringify(this.global.userLocation) }).then(res => {
      console.log(res);
      this.search.address = res.address;
      this.search.latitude = res.lat;
      this.search.longitude = res.lon;
    })
  }

  logout() {
    this.global.storage.query("DELETE FROM smart_auth").then(res => {
      this.navCtrl.setRoot('LoginPage');
    })
  }

}
